import os
import yaml
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_directories(host):
    dirs = [
        "/var/lib/matrix-synapse",
        "/var/log/matrix-synapse",
        "/etc/matrix-synapse",
    ]
    for dir in dirs:
        d = host.file(dir)
        assert d.is_directory


def test_files(host):
    files = [
        "/etc/matrix-synapse/homeserver.yaml",
        "/etc/matrix-synapse/log.yaml",
        "/etc/matrix-synapse/matrix.example.com.tls.crt",
    ]
    for file in files:
        f = host.file(file)
        assert f.exists
        assert f.is_file


def test_service(host):
    s = host.service("matrix-synapse")
    assert s.is_enabled
    assert s.is_running


def test_config_is_valid_yaml(host):
    f = host.file("/etc/matrix-synapse/homeserver.yaml")
    content = f.content_string
    doc = yaml.safe_load(content)
    assert doc["server_name"] == "example.com"
    assert doc["log_config"] == "/etc/matrix-synapse/log.yaml"


def test_log_config_is_valid_yaml(host):
    f = host.file("/etc/matrix-synapse/log.yaml")
    content = f.content_string
    doc = yaml.safe_load(content)
    assert doc["root"]["handlers"] == ["file"]


def test_sockets(host):
    # client
    assert host.socket("tcp://0.0.0.0:8008").is_listening
    # federation
    assert host.socket("tcp://0.0.0.0:8048").is_listening
    # metrics
    assert host.socket("tcp://0.0.0.0:9100").is_listening
